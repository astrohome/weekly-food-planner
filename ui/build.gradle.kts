import com.moowork.gradle.node.NodeExtension
import com.moowork.gradle.node.npm.NpmTask

plugins {
    id("com.moowork.node") version "1.3.1"
}

configure<NodeExtension> {
    version = "10.16.3"
    npmVersion = "6.11.2"
    download = true
    workDir = file("${project.buildDir}/nodejs")
    nodeModulesDir = file("${project.projectDir}")
}

task<NpmTask>("ngBuild") {
  dependsOn("npm_install")

  inputs.file("package.json")
  inputs.file("angular.json")
  inputs.files(fileTree("src"))

  outputs.dir("../api/src/main/resources/static")

  setNpmCommand("run", "build-prod")
}
