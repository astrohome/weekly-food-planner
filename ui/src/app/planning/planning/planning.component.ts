import {Component} from '@angular/core';
import {PlanningService} from "../../services/planning.service";
import {Planning, PlanningRequest} from "../../model/Planning";
import {DishTypeMapping} from "../../model/Dish";
import {TimelineItem} from 'ngx-horizontal-timeline';

@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss']
})
export class PlanningComponent {

  constructor(private planningService: PlanningService) {
    this.planningRequest = new PlanningRequest();
    this.planningRequest.dishType = this.dishTypes[1].value;
    this.planningRequest.servings = 10;
  }

  result: Planning;
  dishTypes = DishTypeMapping;
  planningRequest: PlanningRequest;
  items: TimelineItem[] = [];

  getPlanning() {
    this.planningService.getPlanning(this.planningRequest).subscribe((planning: Planning) => {
      this.result = planning;
      for (const dish of this.result.dishes) {
        this.items.push({
          label: dish.date.toString(),
          icon: 'fa fa-address-book-o',
          expanded: false,
          active: true,
          title: dish.name,
          color: '16a085'
        });
      }
    }, error => {
      console.log(error);
    });
  }

}
