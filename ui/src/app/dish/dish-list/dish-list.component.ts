import { Component, OnInit } from '@angular/core';
import {DishService} from "../../services/dish.service";
import {CreateDishDto, Dish, DishType} from "../../model/Dish";
import {Ingredient} from "../../model/Ingredient";
import {IngredientService} from "../../services/ingredient.service";
import {DishIngredientDto} from "../../model/DishIngredient";

@Component({
  selector: 'app-dish-list',
  templateUrl: './dish-list.component.html',
  styleUrls: ['./dish-list.component.scss']
})
export class DishListComponent implements OnInit {

  dishes: Dish[];
  newDish = new CreateDishDto();
  dishTypes = DishType;
  ingredients: Ingredient[];
  creationErrorMessage: string;
  topErrorMessage: string;

  constructor(private dishService: DishService, private ingredientService: IngredientService) {
  }

  ngOnInit() {
    this.newDish = new CreateDishDto();
    this.newDish.ingredients = [];
    this.newDish.ingredients.push(new DishIngredientDto());

    this.dishService.getDishes().subscribe((dishes: Dish[]) => {
      this.dishes = dishes;
    }, error => {
      console.log(error);
    });
    this.ingredientService.getIngredients().subscribe((ingredients: Ingredient[]) => {
      this.ingredients = ingredients;
    }, error => {
      console.log(error);
    });
  }

  addDish() {
    this.dishService.addDish(this.newDish).subscribe(
      dish => {
        this.dishes.push(dish);
        this.newDish = new CreateDishDto();
        this.newDish.ingredients = [];
        this.newDish.ingredients.push(new DishIngredientDto());
      },
      error => {
        if (error.status === 409) {
          this.creationErrorMessage = "Dish with name " + this.newDish.name + " already exists";
        }
      }
    );
  }

  addIngredient() {
    this.newDish.ingredients.push(new DishIngredientDto())
  }

  deleteDish(dish: Dish) {
      this.dishService.deleteDish(dish.id).subscribe(() => {
          const index = this.dishes.indexOf(dish);
          this.dishes.splice(index, 1);
        },
        error => console.log("error"));
  }

  getIngredients(dish: Dish) {
    return this.dishService.getIngredients(dish);
  }
}
