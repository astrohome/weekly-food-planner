import {ModuleWithProviders, NgModule} from '@angular/core'

import { ToastComponent } from './toast.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [ToastComponent],
  imports: [
    NgbModule,
    CommonModule
  ],
  exports: [
    ToastComponent
  ],
  entryComponents: [ToastComponent]
})
export class ToastModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: ToastModule,
    };
  }
}
