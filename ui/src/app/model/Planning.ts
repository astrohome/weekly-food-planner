import {DishType} from "./Dish";

export class Planning {
  ingredients: PlanningIngredient[];
  dishes: PlanningDish[];
}

export class PlanningIngredient {
  name: string;
  quantity: number;
  measure: string;
}

export class PlanningDish {
  name: string;
  servings: number;
  date: Date;
}

export class PlanningRequest {
  servings: number;
  dishType: DishType;
}
