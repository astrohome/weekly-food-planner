export class Ingredient {
  id: number;
  name: string;
  measure: string;
  usedInDishes: number;
}
