import {DishIngredient, DishIngredientDto} from "./DishIngredient";

export class Dish {
  id: number;
  name: string;
  servings: number;
  type: DishType;
  ingredients: DishIngredient[];
}

export enum DishType {
  vegetarian = 0, meat = 1, fish = 2
}

export const DishTypeMapping = [
  { value: DishType.vegetarian, name: 'Vegetarian' },
  { value: DishType.meat, name: 'Meat' },
  { value: DishType.fish, name: 'Fish' }
];

export class CreateDishDto {
  name: string;
  servings: number;
  ingredients: DishIngredientDto[];
  type: DishType;
}
