export class ToastData {
  text?: string;
  type: ToastType;
}

export type ToastType = 'warning' | 'info' | 'success';
