import {Ingredient} from "./Ingredient";
import {Dish} from "./Dish";

export class DishIngredient {
  ingredient: Ingredient;
  dish: Dish;
  quantity: number;

  constructor(dish: Dish) {
    this.dish = dish;
  }
}

export class DishIngredientDto {
  ingredientId: number;
  quantity: number;
}
