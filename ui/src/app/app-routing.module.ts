import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IngredientListComponent } from "./ingredient/ingredient-list/ingredient-list.component";
import {DishListComponent} from "./dish/dish-list/dish-list.component";
import {PlanningComponent} from "./planning/planning/planning.component";

const routes: Routes = [
  { path:  '', pathMatch:  'full', redirectTo:  'ingredients'},
  { path: 'ingredients', component: IngredientListComponent},
  { path: 'dishes', component: DishListComponent},
  { path: 'planning', component: PlanningComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
