import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {CreateDishDto, Dish} from '../model/Dish';
import { Observable, of, throwError } from 'rxjs';
import { retry, catchError, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
// Base url
const baseurl = environment.productionApiUrl + '/dishes';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor(private http: HttpClient) { }

  getDishes(): Observable<Dish[]> {
    return this.http.get<Dish[]>(baseurl);
  }

  getIngredients(dish: Dish): string {
    return dish.ingredients.map(x => x.ingredient.name + " " + x.quantity.toString() + " " + x.ingredient.measure).join(",")
  }

  addDish(dish: CreateDishDto): Observable<Dish> {
    return this.http.post<Dish>(baseurl, dish, httpOptions);
  }

  deleteDish(id: number) {
    return this.http.delete(baseurl + '/' + id, httpOptions);
  }
}
