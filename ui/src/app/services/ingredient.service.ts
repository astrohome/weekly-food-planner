import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Ingredient } from '../model/Ingredient';
import { Observable, of} from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import {environment} from "../../environments/environment";

// Base url
const baseurl = environment.productionApiUrl + '/ingredients';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class IngredientService {

  constructor(private http: HttpClient) { }

  getIngredients(): Observable<Ingredient[]> {
      return this.http.get<Ingredient[]>(baseurl);
  }

  addIngredient(ingredient: Ingredient): Observable<Ingredient> {
    return this.http.post<Ingredient>(baseurl, ingredient, httpOptions);
  }

  deleteIngredient(id: number) {
    return this.http.delete(baseurl + '/' + id, httpOptions);
  }
}
