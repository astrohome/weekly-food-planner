import {ComponentRef, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {catchError, tap} from "rxjs/operators";
import {Planning, PlanningRequest} from "../model/Planning";
import {environment} from "../../environments/environment";
import {Ingredient} from "../model/Ingredient";

// Base url
const baseurl = environment.productionApiUrl + '/planning';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class PlanningService {

  constructor(private http: HttpClient) { }

  getPlanning(planningRequest: PlanningRequest): Observable<Planning> {
    return this.http.post<Planning>(baseurl, planningRequest, httpOptions);
  }
}
