import { Component, OnInit } from '@angular/core';
import { IngredientService } from '../../services/ingredient.service';
import { Ingredient } from "../../model/Ingredient";

@Component({
  selector: 'app-ingredient-list',
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.scss']
})
export class IngredientListComponent implements OnInit {

  ingredients: Ingredient[];
  newIngredient: Ingredient;
  creationErrorMessage: string;
  topErrorMessage: string;

  constructor(private ingredientService: IngredientService) { }

  ngOnInit() {
    this.ingredientService.getIngredients().subscribe((ingredients: Ingredient[]) => {
      this.ingredients = ingredients;
    }, error => {
      this.topErrorMessage = error.error;
    });
    this.newIngredient = new Ingredient();
  }

  deleteIngredient(ingredient: Ingredient) {
    if (ingredient.usedInDishes === 0)
      this.ingredientService.deleteIngredient(ingredient.id).subscribe(() => {
        const index = this.ingredients.indexOf(ingredient);
        this.ingredients.splice(index, 1);
      },
        error => {
        this.topErrorMessage = error.error;
      });
    else alert("can't delete, it's used in " + ingredient.usedInDishes + "dishes");
  }

  addIngredient(ingredient: Ingredient) {
    this.ingredientService.addIngredient(ingredient).subscribe((created) => {
      this.creationErrorMessage = null;
      this.ingredients.push(created);
      this.newIngredient = new Ingredient();
    }, error => {
      if (error.status === 409) {
        this.creationErrorMessage = "Ingredient already exists";
      }
    })
  }
}
