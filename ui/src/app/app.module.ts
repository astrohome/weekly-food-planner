import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import "reflect-metadata";

import { IngredientListComponent } from './ingredient/ingredient-list/ingredient-list.component';
import { DishListComponent } from './dish/dish-list/dish-list.component';
import { PlanningComponent} from "./planning/planning/planning.component";
import {ServerErrorInterceptor} from "./errors/server-error.interceptor";
import {ToastModule} from "./toast";
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { NgxHorizontalTimelineModule } from 'ngx-horizontal-timeline';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

@NgModule({
  declarations: [
    AppComponent,
    IngredientListComponent,
    DishListComponent,
    PlanningComponent
  ],
  imports: [
    NgbModule,
    NgxHorizontalTimelineModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ToastModule.forRoot(),
    NgBootstrapFormValidationModule.forRoot(),
    NgBootstrapFormValidationModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ServerErrorInterceptor, multi: true }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
