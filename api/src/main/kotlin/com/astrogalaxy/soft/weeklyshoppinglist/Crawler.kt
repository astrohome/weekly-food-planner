package com.astrogalaxy.soft.weeklyshoppinglist

/*
@SpringBootApplication
@EnableTransactionManagement
class Crawler(
    private val dishRepository: DishRepository
) : CommandLineRunner {
    override fun run(vararg args: String?) {
        // Get the content of an URL.
        runBlocking {
           call()
        }
    }

    @KtorExperimentalAPI
    suspend fun call() {
        val client = HttpClient(CIO) {
            install(JsonFeature) {
                serializer = GsonSerializer {
                    serializeNulls()
                    disableHtmlEscaping()
                }
            }
        }

        val ingredientsMap = mutableMapOf<String, MutableList<String>>()

        for (i in 1..10) {
            val wrapper = client.get<Meal>("https://www.themealdb.com/api/json/v1/1/random.php")
            val meal = wrapper.meals[0]
            val ingredients = meal::class.memberProperties.map{ it.name }.filter { it.contains("strIngredient") }
                .toList().sorted()
            val measures = meal::class.memberProperties.map{ it.name }.filter { it.contains("strMeasure") }
                .toList().sorted()

            ingredients.zip(measures).forEach {
                val ingredient = meal.getThroughReflection<String>(it.first)?.toLowerCase()?.trim()
                val measure = meal.getThroughReflection<String>(it.second)?.toLowerCase()?.trim()
                if (ingredient != null && measure != null && !ingredient.isBlank()) {
                    if (ingredientsMap.containsKey(ingredient.toLowerCase())) {
                        ingredientsMap[ingredient]?.add(measure)
                    } else {
                        ingredientsMap[ingredient] = mutableListOf(measure)
                    }
                }
            }
        }
        println(ingredientsMap)
    }
}

inline fun <reified T : Any> Any.getThroughReflection(propertyName: String): T? {
    val getterName = "get" + propertyName.capitalize()
    return try {
        javaClass.getMethod(getterName).invoke(this) as? T
    } catch (e: NoSuchMethodException) {
        null
    }
}
/*
fun main(args: Array<String>) {
    SpringApplicationBuilder(Crawler::class.java)
        .web(WebApplicationType.NONE)
        .run(*args)
}*/