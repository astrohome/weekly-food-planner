package com.astrogalaxy.soft.weeklyshoppinglist.dao

import com.astrogalaxy.soft.weeklyshoppinglist.model.Dish
import com.astrogalaxy.soft.weeklyshoppinglist.model.DishType
import com.astrogalaxy.soft.weeklyshoppinglist.model.Ingredient
import org.springframework.data.repository.CrudRepository
import java.time.LocalDate


interface DishRepository : CrudRepository<Dish, Int> {
    fun existsByName(name: String): Boolean
    fun findAllByType(type: DishType): List<Dish>
}

interface IngredientRepository : CrudRepository<Ingredient, Int> {
    fun existsByName(name: String): Boolean
}

interface PlanningRepository : CrudRepository<Dish, LocalDate> {

}