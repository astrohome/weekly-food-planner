package com.astrogalaxy.soft.weeklyshoppinglist

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.transaction.annotation.EnableTransactionManagement

@SpringBootApplication
@EnableTransactionManagement
class WeeklyShoppingListApplication

fun main(args: Array<String>) {
    runApplication<WeeklyShoppingListApplication>(*args)
}
