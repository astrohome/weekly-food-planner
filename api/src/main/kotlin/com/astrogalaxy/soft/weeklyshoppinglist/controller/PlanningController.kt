package com.astrogalaxy.soft.weeklyshoppinglist.controller

import com.astrogalaxy.soft.weeklyshoppinglist.dao.DishRepository
import com.astrogalaxy.soft.weeklyshoppinglist.model.*
import org.springframework.web.bind.annotation.*
import java.time.LocalDate
import javax.validation.Valid
import kotlin.math.ceil

@RestController
@RequestMapping("/api/planning")
class PlanningController(
    private val dishRepository: DishRepository
) {
    @CrossOrigin(origins = ["http://localhost:4200"])
    @PostMapping
    fun getPlanning(@RequestBody @Valid planningRequest: PlanningRequest): Planning {
        val dishPlan = p(planningRequest.dishType, planningRequest.servings, LocalDate.now())

        val ingredients = dishPlan.map { dish -> dish.ingredients.map { it }}
            .flatten()
            .groupBy { it.ingredient }
            .map { PlanningIngredient(it.key.name, it.value.sumBy { dishIngredient -> dishIngredient.quantity }, it.key.measure) }
        val dishes = dishPlan.map { PlanningDish(it.name, it.servings) }

        val dates = mutableListOf<LocalDate>()

        for (dish in dishes) {
            // TODO: config how many portions per day
            val days = dish.servings / 2.toDouble()
            dates.add(planningRequest.startDate.plusDays(ceil(days).toLong()))
        }
        val dishesWithDates = dates.zip(dishes).map { it.second.applyDate(it.first) }.toList()

        return Planning(ingredients = ingredients, dishes = dishesWithDates)
    }

    private fun p(type: DishType, desiredPortions: Int, startDate: LocalDate): List<Dish> {
        val dishes = dishRepository.findAllByType(type).toMutableList()
        return plan(dishes, desiredPortions, startDate)
    }

    private fun plan(dishes: List<Dish>, portions: Int, date: LocalDate): List<Dish> {
        if (dishes.isEmpty() || portions <= 0) return listOf()

        val dish = dishes.random()
        val portionsLeft = portions - dish.servings
        val days = dish.servings / 4.toDouble()
        return listOf(dish) + plan(dishes - dish, portionsLeft, date.plusDays(ceil(days).toLong()))
    }
}