package com.astrogalaxy.soft.weeklyshoppinglist.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping



@Controller
class ForwardingController {
    @RequestMapping(value = ["/{:[^\\\\.]*}"])
    fun redirect(): String {
        // Forward to home page so that route is preserved.
        return "forward:/"
    }
}