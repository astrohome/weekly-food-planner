package com.astrogalaxy.soft.weeklyshoppinglist.model

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonManagedReference
import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.NaturalId
import org.hibernate.annotations.NaturalIdCache
import java.io.Serializable
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
data class Dish(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Int = 0,

    @Column(name = "name", nullable = false)
    @NotBlank(message = "Name is mandatory")
    val name: String,

    @Column
    val servings: Int,

    @Column
    val type: DishType = DishType.meat,

    @OneToMany(
        mappedBy = "dish",
        cascade = [CascadeType.ALL],
        orphanRemoval = true
    )
    val ingredients: MutableSet<DishIngredient> = mutableSetOf()

    ) : Serializable {

    override fun toString(): String {
        return "Dish(id=$id, name=$name, servings=$servings)"
    }

    override fun hashCode(): Int {
        return id
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Dish) return false
        return Objects.equals(this.id, other.id)
    }
}

@Entity
@NaturalIdCache
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
data class Ingredient(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    val id: Int = 0,

    @NaturalId
    @Column(name = "name", nullable = false)
    @NotBlank(message = "Name is mandatory")
    val name: String,

    @NotBlank(message = "Measure is mandatory")
    val measure: String
) : Serializable {

    @JsonBackReference
    @OneToMany(
        mappedBy = "ingredient",
        cascade = [CascadeType.ALL],
        orphanRemoval = true
    )
    val dishes: MutableSet<DishIngredient> = mutableSetOf()

    @JsonProperty("usedInDishes")
    fun getUsedInDishes(): Int {
        return dishes.size
    }
}

@Embeddable
data class DishIngredientId(
    @Column(name = "post_id")
    val dishId: Int = 0,
    @Column(name = "ingredient_id")
    val ingredientId: Int = 0
) : Serializable

@Entity
@Table(name = "dish_ingredient")
data class DishIngredient(
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("dishId")
    val dish: Dish,

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("ingredientId")
    val ingredient: Ingredient,

    val quantity: Int
) : Serializable {
    @EmbeddedId
    private val id: DishIngredientId = DishIngredientId(dish.id, ingredient.id)
}

data class DishIngredientDto(
    val ingredientId: Int,
    val quantity: Int
)

data class CreateDishDto(
    val name: String,
    val servings: Int,
    val type: DishType,
    val ingredients: List<DishIngredientDto>
)

enum class DishType {
   vegetarian, meat, fish
}

data class PlanningIngredient(
    val name: String,
    val quantity: Int,
    val measure: String
)

data class PlanningDish(
    val name: String,
    val servings: Int,
    val date: LocalDate = LocalDate.now()
)

fun PlanningDish.applyDate(date: LocalDate): PlanningDish {
    return PlanningDish(name = this.name, servings = this.servings, date = date)
}


data class PlanningRequest(
    val startDate: LocalDate = LocalDate.now(),
    val servings: Int,
    val dishType: DishType = DishType.meat
)

data class Planning(
    val ingredients: List<PlanningIngredient>,
    val dishes: List<PlanningDish>
)
