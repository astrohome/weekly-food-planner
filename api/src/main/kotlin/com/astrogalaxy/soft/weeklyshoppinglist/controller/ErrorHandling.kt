package com.astrogalaxy.soft.weeklyshoppinglist.controller

import com.astrogalaxy.soft.weeklyshoppinglist.model.DishNameAlreadyExistsException
import com.astrogalaxy.soft.weeklyshoppinglist.model.IngredientNameAlreadyExistsException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ErrorHandling : ResponseEntityExceptionHandler() {

    @ExceptionHandler(IngredientNameAlreadyExistsException::class, DishNameAlreadyExistsException::class)
    protected fun ingredientNameAlreadyExistsException(ex: RuntimeException, request: WebRequest) : ResponseEntity<Any> {
        val bodyOfResponse = "Entity already exists (id=${ex.message})"
        return handleExceptionInternal(
            ex, bodyOfResponse,
            HttpHeaders(), HttpStatus.CONFLICT, request
        )
    }
}