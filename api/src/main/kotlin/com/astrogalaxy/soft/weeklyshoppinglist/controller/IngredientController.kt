package com.astrogalaxy.soft.weeklyshoppinglist.controller

import com.astrogalaxy.soft.weeklyshoppinglist.dao.IngredientRepository
import com.astrogalaxy.soft.weeklyshoppinglist.model.Ingredient
import com.astrogalaxy.soft.weeklyshoppinglist.model.IngredientIsUsedInDishes
import com.astrogalaxy.soft.weeklyshoppinglist.model.IngredientNameAlreadyExistsException
import com.astrogalaxy.soft.weeklyshoppinglist.model.IngredientNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/ingredients")
class IngredientController(
    private val ingredientRepository: IngredientRepository
) {
    @CrossOrigin(origins = ["http://localhost:4200"])
    @GetMapping
    fun getAll(): Iterable<Ingredient> {
        return ingredientRepository.findAll()
    }

    @CrossOrigin(origins = ["http://localhost:4200"])
    @PostMapping
    fun addIngredient(@RequestBody @Valid ingredient: Ingredient): Ingredient {
        if(ingredientRepository.existsByName(ingredient.name)) {
            throw IngredientNameAlreadyExistsException(ingredient.name)
        }
        return ingredientRepository.save(ingredient)
    }

    @CrossOrigin(origins = ["http://localhost:4200"])
    @PatchMapping
    fun editIngredient(@RequestBody @Valid ingredient: Ingredient): Ingredient {
        if(!ingredientRepository.existsById(ingredient.id)) {
            throw IngredientNotFoundException(ingredient.name)
        }
        return ingredientRepository.save(ingredient)
    }

    @CrossOrigin(origins = ["http://localhost:4200"])
    @DeleteMapping("/{id}")
    fun deleteIngredient(@PathVariable("id") id: Int): ResponseEntity<Any> {
        val ingredient = ingredientRepository.findById(id)
        if (ingredient.isPresent) {
            if (ingredient.get().dishes.isEmpty()) {
                ingredientRepository.deleteById(id)
                return ResponseEntity(HttpStatus.NO_CONTENT)
            } else {
                throw IngredientIsUsedInDishes(ingredient.get().dishes.map { it.dish.name }.toString())
            }
        } else {
            throw IngredientNotFoundException(id.toString())
        }
    }
}