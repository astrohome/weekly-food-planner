package com.astrogalaxy.soft.weeklyshoppinglist.controller

import com.astrogalaxy.soft.weeklyshoppinglist.dao.DishRepository
import com.astrogalaxy.soft.weeklyshoppinglist.dao.IngredientRepository
import com.astrogalaxy.soft.weeklyshoppinglist.model.*
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/dishes")
class DishController(
    private val dishRepository: DishRepository,
    private val ingredientRepository: IngredientRepository
) {
    @CrossOrigin(origins = ["http://localhost:4200"])
    @GetMapping
    fun getAll(): Iterable<Dish> {
        return dishRepository.findAll()
    }

    @CrossOrigin(origins = ["http://localhost:4200"])
    @PostMapping
    fun addNew(@RequestBody dish: CreateDishDto): Dish {
        if(dishRepository.existsByName(dish.name)) {
            throw DishNameAlreadyExistsException(dish.name)
        }
        if (dishRepository.findAllById(dish.ingredients.map { it.ingredientId }).any { it == null }) {
            throw IngredientNotFoundException("not")
        }
        val initial = dishRepository.save(Dish(name = dish.name, servings = dish.servings))
        initial.ingredients.addAll(dish.ingredients.map { DishIngredient(dish = initial,
            quantity = it.quantity,
            ingredient = ingredientRepository.findById(it.ingredientId).get()) })
        return dishRepository.save(initial)
    }

    @CrossOrigin(origins = ["http://localhost:4200"])
    @DeleteMapping("/{id}")
    fun deleteDish(@PathVariable("id") id: Int): ResponseEntity<Any> {
        val dish = dishRepository.findById(id)
        if (dish.isPresent) {
            dishRepository.deleteById(id)
            return ResponseEntity(HttpStatus.NO_CONTENT)
        } else {
            throw DishNotFoundException(id.toString())
        }
    }
}