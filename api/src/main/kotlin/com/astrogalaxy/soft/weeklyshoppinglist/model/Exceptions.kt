package com.astrogalaxy.soft.weeklyshoppinglist.model

data class IngredientNameAlreadyExistsException(override val message: String) : RuntimeException(message)

data class IngredientNotFoundException(override val message: String) : RuntimeException(message)

data class IngredientIsUsedInDishes(override val message: String) : RuntimeException(message)

data class DishNameAlreadyExistsException(override val message: String) : RuntimeException(message)

data class DishNotFoundException(override val message: String) : RuntimeException(message)